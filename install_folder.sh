#!/bin/bash

cat >> /etc/bash.bashrc <<EOL
  [[ -s "/etc/mybash/.profile" ]] && source "/etc/mybash/.profile"
EOL

SOURCE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
mkdir /etc/mybash/
cp $SOURCE_DIR/.profile /etc/mybash/