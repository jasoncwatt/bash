#!/bin/bash

if grep -q color /etc/gitconfig; then
  echo "already has colors"
else
cat >> /etc/gitconfig <<EOL
[color]
  ui = auto
[color "branch"]
  current = yellow reverse
  local = blue
  remote = green
[color "diff"]
  meta = yellow bold
  frag = magenta bold
  old = red bold
  new = green bold
[color "status"]
  added = green
  changed = blue
  untracked = red
[alias]
  ls = log --pretty=format:"%C(yellow)%h%Cred%d\\ %Creset%s%Cblue\\ [%cn]" --decorate
  ll = log --pretty=format:"%C(yellow)%h%Cred%d\\ %Creset%s%Cblue\\ [%cn]" --decorate --numstat
  ld = log --pretty=format:"%C(yellow)%h\\ %ad%Cred%d\\ %Creset%s%Cblue\\ [%cn]" --decorate --date=relative
  le = log --oneline --decorate
  fl = log -u
  dl = "!git ll -1"
  dlc = diff --cached HEAD^
  f = "!git ls-files | grep -i"
  grep = grep -Ii
  gr = grep -Ii
  la = "!git config -l | grep alias | cut -c 7-"
  ours = "!f() { git co --ours $@ && git add $@; }; f"
  theirs = "!f() { git co --theirs $@ && git add $@; }; f"
EOL
fi